# api

## ENDPOINT

- Obtener todas los negocios (GET)

> https://api-apoyoperu.herokuapp.com/businesses

- Obtener solo un negocio por id enviado en `body` de `request` (GET)

> https://api-apoyoperu.herokuapp.com/business

ejemplo:

```javascript
{
	"_id": "5e89342afed3466743437372"
}

```

- Crear un nuevo negocio enviando los datos en el `body` del `request` (POST)

> https://api-apoyoperu.herokuapp.com/business

```javascript
{
	"name": "Darwin mercados",
	"email": "g@g.com",
	"cellphone": "3333",
	"phone": "1111",
	"identification": "1111",
	"description": "2222",
	"person": "darwin",
	"department": "LIMA",
	"address": "ddd",
	"coordinates": {
		"lat": 11.11,
		"log": 23.33
	},
	"area":[{
		"name": "TIC",
		"activities": ["desarrollo", "reparacion"]
	}]

}

```
