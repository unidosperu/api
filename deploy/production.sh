#!/bin/sh
set -a
. ./.env
set +a

docker login -u "$CI_DEPLOY_USER" -p "$CI_DEPLOY_TOKEN" $CI_REGISTRY || exit 1;
docker pull $IMAGE_DOCKER:$TAG_DOCKER || exit 1;

echo "Delete containers"
docker-compose down || exit 1
echo "Start containers"
docker-compose up -d

docker logout $CI_REGISTRY
rm -f .env || exit 1;
