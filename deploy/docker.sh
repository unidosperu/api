#!/bin/sh
cd deploy
set -a
. ../.env
set +a
cd ..
docker-compose -f ./docker-compose.stg.yml down || exit 1

test ! -z "$(docker images -q $IMAGE_DOCKER)" && docker rmi $IMAGE_DOCKER && echo "image removed"
#test ! -z "$(docker ps -aqf name=$CONTAINER_DOCKER)" && docker rm -f $CONTAINER_DOCKER && echo "container removed"
npm run build
docker build -t $IMAGE_DOCKER:$TAG_DOCKER .

docker-compose -f ./docker-compose.stg.yml up || exit 1
