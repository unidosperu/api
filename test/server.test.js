/* eslint-disable */
'use strict'
import chai from 'chai';
//import request from "request";
import chaiHttp from 'chai-http'
import server from '@app/server'

//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let should = chai.should();

//https://github.com/Automattic/expect.js

chai.use( chaiHttp );
describe( 'Status and content', () => {
  describe ( 'Init server', () => {
    it( 'status', ( done ) => {
      chai.request( server )
        .get( '/user' )
        .end( ( error, res ) => {
					//console.log(res)
          //res.should.have.status( 200 );
          //res.should.not.have.status( 400 );
          res.body.should.be.a( 'object' );
          //chai.expect(res.body.msg).to.equal('Hello World');
          //should(res.body).have.property('msg','Hello World')
          if( error ) done( error )
          done()
        } );
    } );
  } );


  it( 'Page not Found', ( done ) => {
    chai.request( server )
      .get( '/other' )
      .end( ( error, res ) => {
        res.should.have.status( 404 );
        if( error ) done( error )
        done()
      } );
  } );


} );


/**
 *
 *  request('http://localhost:4000/', (error, res, body)=> {
                //res.status.should.equal(400)
                console.log(res.statusCode)
               expect(res.statusCode).to.equal(200)
               expect(res.statusCode).to.not.equal(400)
               if(error) done(error)
               done()

            });
 *
 * describe('Status and content', ()=> {
    describe ('Main page', ()=> {
        it('status', (done)=>{
            request('http://localhost:4000/', (error, res, body)=> {
                const result = res.statusCode
                (res.statusCode).should.equal(200)
               // expect(res.statusCode).to.equal(200);

            });
        });

    it('content', (done)=> {
          request('http://localhost:4000/' , (error, res, body)=> {
              console.log(body)
              expect(body.msg).to.equal('Hello World');
            });
        });
    });


describe ('About page', ()=> {
        it('status', (done)=>{
            request('http://localhost:4000/about', (error, res, body)=> {
                console.log(res.statusCode)
                expect(res.statusCode).to.equal(200);
            });
        });
});
});

 */
