export const jwt_key = process.env.JWT_SECRET || "12345f@cil";
export const port = process.env.PORT || 4000;
export const node_env = process.env.NODE_ENV || "dev";
const {
	MONGO_USERNAME,
	MONGO_PASSWORD,
	MONGO_HOSTNAME,
	MONGO_PORT,
	MONGO_DB,
} = process.env;

export const db = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;
