import { Business } from '@models';
import mongoose from 'mongoose';

//GET ALL BUSINESSES
const getBusinesses = async (req, res) => {
	try {
		const businesses = await Business.find();

		return res.status(200).send({
			response: {
				data: businesses
			},
			status: true
		});
	} catch (error) {
		console.log(`Error get Business => ${error}`);
		return res.status(500).send({
			response: {
				data: [],
				msg: `Error get Business => ${error}`
			},
			status: false
		});
	}
};

//GET ONE BUSINESS BY ID
const getBusiness = async (req, res) => {
	try {
		const { _id } = req?.body;

		if (!_id) {
			return res.status(400).send({
				response: {
					msg: 'Empty fields'
				},
				status: false
			});
		}

		if (!mongoose.Types.ObjectId.isValid(_id)) {
			return res.status(403).send({
				response: {
					msg: 'Invalid id'
				},
				status: false
			});
		}

		const business = await Business.findById(_id);

		if (!business) {
			return res.status(404).send({
				response: {
					data: null,
					msg: 'Business not found'
				},
				status: false
			});
		}
		return res.status(200).send({
			response: {
				data: business
			},
			status: true
		});
	} catch (error) {
		console.log(`Error get Business => ${error}`);
		return res.status(500).send({
			response: {
				data: [],
				msg: `Error get Business => ${error}`
			},
			status: false
		});
	}
};

// POST BUSINESS
const postBusiness = async (req, res) => {
	const {
		name,
		email,
		phone,
		cellphone,
		identification,
		description,
		person,
		department,
		address,
		coordinates,
		area
	} = req?.body ?? {};

	if (
		!name ||
		!email ||
		!phone ||
		!cellphone ||
		!identification ||
		!description ||
		!person ||
		!department ||
		!address ||
		!coordinates ||
		!area
	) {
		return res.status(400).send({ msg: 'Empty fields', status: false });
	}
	// Check if there is a user with the same email
	const exist_email = await Business.findOne({ email });
	console.log(exist_email);
	if (exist_email) {
		return res
			.status(403)
			.send({ msg: 'Email is already in use', status: false });
	}

	const args = {
		email,
		name,
		phone,
		cellphone,
		identification,
		description,
		person,
		department,
		address,
		coordinates,
		area
	};
	const _business = new Business(args);
	const business = await _business.save();
	if (!business)
		return res.status(500).send({ msg: 'Business not created', status: false });

	return res.status(200).send({ msg: 'Business created', status: true });
};

export { getBusinesses, getBusiness, postBusiness };

/**
 * 		try {
			const { email, password, name, kind } = req.body;
			if (!email || !password || !name) {
				return res.status(400).send({ msg: 'Empty fields', status: false });
			}
			// Check if there is a user with the same email
			const existUser = await User.findOne({ email });
			if (existUser)
				return res
					.status(403)
					.send({ msg: 'Email is already in use', status: false });
			const user = new User({ email, name, password, kind });
			const newUser = await user.save();
			if (!newUser)
				return res.status(500).send({ msg: 'User not created', status: false });

			const token = await createToken(newUser, jwt_key);
			if (token) res.status(200).send({ status: true, token });
			else res.status(409).send({ msg: 'Token not generate', status: true });
		} catch (error) {
			console.log(error);
			return res
				.status(500)
				.send({ msg: `Internal server error ${error}`, status: false });
		}
 */
