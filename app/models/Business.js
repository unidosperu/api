'use strict';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
// import moment from 'moment-timezone';

// const kind = ['GUEST', 'ADMIN', 'STUDENT', 'TUTOR'];

const BusinessSchema = new Schema(
	{
		name: {
			type: String
		},
		phone: {
			type: String
		},
		cellphone: {
			type: String
		},
		email: {
			type: String,
			unique: true,
			lowercase: true,
			match: [/\S+@\S+\.\S+/, 'is invalid']
		},
		identification: {
			type: String
		},
		description: {
			type: String
		},
		person: {
			type: String
		},
		department: {
			type: String
		},
		address: {
			type: String
		},
		coordinates: {
			type: {
				lat: Number,
				lon: Number
			}
		},
		area: {
			type: {
				name: String,
				activities: [
					{
						_id: false,
						type: String
					}
				]
			},
			default: []
		}
		// kind: {
		// 	type: String,
		// 	enum: kind,
		// 	uppercase: true,
		// 	require: true,
		// 	default: 'GUEST'
		// },
	},
	{
		timestamps: true
	}
);

let Model = mongoose.model('Business', BusinessSchema);
module.exports = Model;
