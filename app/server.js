'use strict';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';
import business from '@routers/business';

const server = express();
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(helmet());
server.use(morgan('dev'));
const corsOptions = {
	origin: '*',
	methods: '*',
	allowedHeaders: ['Content-Type', 'Authorization']
};
server.use(cors(corsOptions));

// ENDPOINTS
server.use('/', business);

//ENDPOINT 404
server.get('*', (req, res) => res.status(404).send({ msg: 'Page not Found' }));

export default server;
