'use strict';
import express from 'express';

import {
	getBusinesses,
	getBusiness,
	postBusiness
} from '@controllers/business';

let router = express.Router();
router.get('/businesses', getBusinesses);
router.get('/business', getBusiness);
router.post('/business', postBusiness);

export default router;
