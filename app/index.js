import server from './server';
import db from './db';

import { port, db as connection } from '@app/config';

//ENDPOINT 404
server.get('*', (req, res) => res.status(404).send({ msg: 'Page not Found' }));

db.connect(connection, {
	useNewUrlParser: true,
	useCreateIndex: true,
	useUnifiedTopology: true
})
	.then(() => {
		console.log('MONGODB is running at connection');
		server.listen(port, () => {
			console.log(`SERVER is listening on PORT => ${port}`);
			console.log(`Access to http://127.0.0.1:${port}`);
		});
	})
	.catch((err) => console.log(`${err}`));
