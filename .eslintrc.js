module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	extends: ["eslint:recommended", "plugin:prettier/recommended"],
	parser: "babel-eslint",
	parserOptions: {
		sourceType: "module",
	},
	rules: {
		"comma-dangle": ["off"],
		quotes: [2, "single", "avoid-escape"],
		"no-console": process.env.NODE_ENV === "production" ? "error" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
	},
	overrides: [
		{
			files: ["*.yml"],
			rules: {
				indent: [2, 2],
			},
		},
	],
};
