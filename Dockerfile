FROM node:10
WORKDIR /app
COPY ./package*.json ./
COPY ./api/ ./api/
RUN npm i --only=production
RUN ls
CMD ["npm","start"]
